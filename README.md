# Posting Opp Details in Chatter and SSO

## Demo (~1:30)

Here's a link to a demo video for both parts of the task: https://www.youtube.com/watch?v=aU9VNMMxWG4

## Task 1: Post Opportunity Details to Account Chatter

### Overview

To make the chatter post, I chose to use a record-triggered flow on create for Opportunity. I ultimately created two versions of the flow:

1. Using the built-in flow action Chatter Post
2. Using a custom action I built in Apex

The built-in Chatter Post action only allows us to post with plain-text. If the user wants to use more advanced markup, they should use the version with the custom action.

![Overall Flow](media/flow-html.png)

### Flow Details

![Flow Details](media/flow-detailed.png)

## Task 2: Single Sign-on Using External Provider

### Overview

This component can be seen working in the demo video linked at the top of this document. Below is a screenshot of the org's SAML settings and the SAML request sent via Axiom

![SSO Saml Settings](media/SAML-SSO-settings.png)

![Axiom Request](media/axiom-settings.png)