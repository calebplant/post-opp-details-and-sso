public with sharing class PostOppToChatter {

@InvocableMethod(label='Post Opportunity Details to Chatter'
                description='Post a rich-text message to an Opportunity\'s parent Account feed')

    public static void execute (List<Requests> requestList)
    {
        System.debug('START PostOppToChatter');

        if(requestList.size() > 0) {
            // System.debug(requestList[0]);
            // System.debug(buildRichTextBody(requestList[0].opp));
            insert new FeedItem(ParentId=requestList[0].opp.AccountId,
                                Body=buildRichTextBody(requestList[0].opp),
                                IsRichText=true);
        }
        System.debug('END PostOppToChatter');
    }

    public class Requests
    {
        @InvocableVariable(label='New Opportunity' description='Opportunity whose details you wish to post.' required=true)
        public Opportunity opp; 
    }

    private static String buildRichTextBody(Opportunity opp)
    {
        String result = '';
        result += addRecordLink(opp.Id, opp.Name);
        result += addBodyLine(opp, 'Amount: ', 'Amount');
        result += addBodyLine(opp, 'Close Date: ', 'CloseDate');
        result += addBodyLine(opp, 'Stage Name: ', 'StageName');
        result += addBodyLine(opp, 'Next Step: ', 'NextStep');
        result += addBodyLine(opp, 'Description: ', 'Description');
        return result;
    }

    private static String addRecordLink(Id oppId, String oppName)
    {
        String recordUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + oppId;
        return '<p><b>Name:</b> <a href="' + recordUrl  +'">' + oppName + '</a></p>';
    }

    private static String addBodyLine(Opportunity opp, String label, String fieldName)
    {
        String result = '';
        if(opp.get(fieldName) != null) {
            String fieldValue = String.valueOf(opp.get(fieldName));

            // Trim time component off close date if present
            if(opp.get(fieldName) instanceof DateTime) {
                DateTime d = (DateTime)opp.get(fieldName);
                fieldValue = d.format('MM/dd/yyyy');
            }

            result = '<p><b>' + label + '</b> ' + fieldValue + '</p>';
        }
        return result;
    }
}